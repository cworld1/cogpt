# Contributing to CoGPT

Thank you for your interest in contributing to CoGPT! We welcome contributions from the community to help improve our project.

This page contains information about how to contribute to CoGPT. Please read it carefully before you start contributing.

## Creating an Issue

If you find a bug or have a feature request, please create an issue in the repository using the appropriate template. This will help us to understand the problem and provide a solution.

The title of the issue should be **in English** and the content can be **in English or Chinese**.

Before contributing, it's also recommended to create an issue to discuss the changes you want to make. This will allow the maintainers to provide feedback and guidance on your contribution.

## Getting Started

### Prerequisites

Before you start contributing, you will need to have the following tools installed on your machine:

- Git
- Go
- make

### Procss to contribute

1. Make sure you have done [Prerequisites](#prerequisites)
2. Fork the repository and clone it to your local machine.
3. Run `make all` to build the project to make sure everything is working.
4. It's recommended to create an issue before starting to work on a feature or a bug fix. This will allow the maintainers to provide feedback and guidance on your contribution.
5. Make your changes or additions to the codebase but please read [Code Conventions](#code conventions) before doing so.
8. Commit your changes and push them to your forked repository.
9. Submit a pull request to the main repository and make sure to pass all the checks.
10. Wait for the review and approval of your pull request.
11. Once your pull request is approved, it will be merged into the main repository.
12. Congratulations! You have successfully contributed to CoGPT.

### How to build and run

We use `make` to build or run the project. Here are some common commands you can use. For detailed information, you can check the `Makefile`. Here is only a brief introduction.

Common commands:

- `make` or `make run` - build and run `cogpt-api` service
- `make all` - build all binaries (`cogpt-api`, `cogpt-get-apptoken`, `gen-share-token`)
- `make tests` - run all tests

We use 3 variables to control the build and run process.

- `exeargs` - arguments for running `cogpt-api`
- `args` - arguments for building
- `args1` - arguments for building
- `args2` - arguments for building

Examples:

```bash
# equivalent to build cogpt-api and run "./cogpt-api -h"
make exeargs="-h"

# equivalent to "GOARCH=amd64 go build -o cogpt-api ./cmd/cogpt-api"
make cogpt-api args1="GOARCH=amd64" args="-o cogpt-api"
```

## Code Conventions

### Style Guide

Use `go fmt` to format your code before submitting a pull request (if you are using VSCode, this will be done automatically).

### Documentation

If you are adding new features or making changes to the existing codebase, please make sure to update the documentation accordingly.

### Testing

It's highly recommended to add unit tests for your changes to ensure the correctness of your code though it's not mandatory.

## Contact

If you have any questions or need further assistance, feel free to reach out to us at [me@geniucker.top](mailto:me@geniucker.top).

We appreciate your contributions and look forward to working with you!
