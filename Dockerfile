FROM --platform=$BUILDPLATFORM golang:alpine as builder

ARG TARGETPLATFORM
ARG BUILDPLATFORM

ENV CGO_ENABLED=0 GOOS=linux

WORKDIR /app

COPY . .

RUN --mount=type=cache,target=/root/.cache/go-build,sharing=locked \
    --mount=type=cache,target=/go/pkg,sharing=locked \
    apk add --update-cache ca-certificates tzdata make && \
    go mod download && \
    \
    if [ "$TARGETPLATFORM" = "linux/amd64" ]; then \
        make cogpt-api args1="GOARCH=amd64" args="-o cogpt-api"; \
    elif [ "$TARGETPLATFORM" = "linux/arm64" ]; then \
        make cogpt-api args1="GOARCH=arm64" args="-o cogpt-api"; \
    elif [ "$TARGETPLATFORM" = "linux/arm/v7" ]; then \
        make cogpt-api args1="GOARCH=arm" args="-o cogpt-api"; \
    else \
        echo "Unsupported platform: $TARGETPLATFORM"; \
        exit 1; \
    fi

FROM scratch

WORKDIR /app

COPY --from=builder /app/cogpt-api /app/cogpt-api
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

EXPOSE 8080

VOLUME ["/app/db", "/app/log"]

ENTRYPOINT ["./cogpt-api"]
