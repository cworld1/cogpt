package init

import (
	"fmt"
)

func init() {
	// initialize config instance
	initConfigInstance()

	// initialize log
	initLog()

	// initialize config
	initConfigEnv()
	initConfig()

	// update log level
	updateLogLevel("")

	// initialize proxy
	initProxy()

	// initialize cache
	initCache()

	fmt.Println()
}
