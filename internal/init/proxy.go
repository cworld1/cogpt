package init

import (
	"cogpt/internal/config"
	"cogpt/internal/log"
	"cogpt/internal/proxy"
)

func initProxy() {
	_, err := proxy.GenProxyClient(config.ConfigInstance.Proxy, 0)
	if err != nil {
		log.Logger.Warn().Str("type", "initProxy").Msg("Invalid proxy address, skip")
		// reset proxy address
		config.ConfigInstance.Proxy = ""
	}
}
