package init

import (
	"cogpt/internal/cache"
	"cogpt/internal/config"
	"cogpt/internal/log"
)

func initCache() {
	cache.CacheInstance.Init(config.ConfigInstance.Cache, config.ConfigInstance.CachePath)

	log.Logger.Info().
		Str("type", "initCache").
		Msg("Initialized cache")
}
