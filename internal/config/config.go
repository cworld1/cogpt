package config

// default value
const (
	DefaultHost          = "localhost"
	DefaultPort          = 8080
	DefaultCache         = true
	DefaultCachePath     = "db/cache.sqlite3"
	DefaultDebug         = false
	DefaultLogLevel      = "info"
	DefaultShareTokenStr = ""
	DefaultProxy         = ""
	DefaultCertFile      = ""
	DefaultKeyFile       = ""
)

var ConfigInstance Config = Config{}

type Config struct {
	Host       string
	Port       int
	Cache      bool
	CachePath  string
	Debug      bool
	LogLevel   string
	ShareToken map[string]string
	Proxy      string
	CertFile   string
	KeyFile    string
}
