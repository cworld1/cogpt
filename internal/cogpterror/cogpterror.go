package cogpterror

type ErrorCode int

const (
	ERR_UNKNOWN ErrorCode = iota
	ERR_CACHE_ITEM_NOT_EXISTS
	ERR_FAILED_TO_CREATE_HTTP_CLIENT
	ERR_FAILED_TO_FINISH_HTTP_REQUEST
	ERR_FAILED_TO_READ_HTTP_RESPONSE_BODY
	ERR_FAILED_TO_GET_COPILOT_TOKEN
	ERR_FAILED_TO_PARSE_JSON
	ERR_INVALID_PROXY_ADDRESS
	ERR_CREATE_PROXY_DIALER
	ERR_AUTH_PENDING
	ERR_EXPIRED_TOKEN
	ERR_REQUEST_TIMEOUT
	ERR_MARSHAL_JSON
	ERR_UNKNOWN_PROXY_TYPE
)

type Error interface {
	Error() string
	Code() ErrorCode
	Type() string
	SetCode(ErrorCode) Error
	SetType(string) Error
}

type CoErr struct {
	ErrCode ErrorCode
	Msg     string
	Typ     string
}

func (e *CoErr) Error() string {
	return e.Msg
}

func (e *CoErr) Code() ErrorCode {
	return e.ErrCode
}

func (e *CoErr) Type() string {
	return e.Typ
}

func (e *CoErr) SetCode(code ErrorCode) Error {
	e.ErrCode = code
	return e
}

func (e *CoErr) SetType(typ string) Error {
	e.Typ = typ
	return e
}
