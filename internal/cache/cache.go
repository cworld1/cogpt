package cache

import (
	myErr "cogpt/internal/cogpterror"
	"cogpt/internal/log"
	"cogpt/internal/util"
	"github.com/jmoiron/sqlx"
	_ "modernc.org/sqlite"
	"os"
	"time"
)

var CacheInstance Cache = Cache{}

const (
	SESSIONID_TTL int64 = 60 * 15          // 15 minutes
	CLEAN_TIMEOUT int64 = 60 * 60 * 24 * 7 // 7 days
)

type Cache struct {
	cache      bool
	cache_path string
	db         *sqlx.DB
	data       map[string]Item
}

func (c *Cache) Init(cache bool, cache_path string) {
	c.cache = cache
	c.cache_path = cache_path
	c.connect()
	c.clean()
}

// @description Connect to the database or create a cache map
func (c *Cache) connect() {
	if c.cache && c.db == nil {
		// create cache directory if not exists
		if err := util.MkdirAllIfNotExists(c.cache_path, os.ModePerm); err != nil {
			log.Logger.Error().Str("type", "Cache.connect").Msgf("Failed to create cache directory: %s", c.cache_path)
			panic(err)
		}

		// connect to database
		var err error
		c.db, err = sqlx.Connect("sqlite", c.cache_path)
		if err != nil {
			log.Logger.Error().Str("type", "Cache.connect").Msgf("Failed to connect to database: %s", c.cache_path)
			panic(err)
		}
		log.Logger.Debug().Str("type", "Cache.connect").Msgf("Connected to database: %s", c.cache_path)

		// create table if not exists
		_, err = c.db.Exec(`
			CREATE TABLE IF NOT EXISTS cache(
				app_token TEXT PRIMARY KEY,
				c_token TEXT DEFAULT '',
				expires_at INTEGER DEFAULT 0,
				vscode_machineid TEXT DEFAULT '',
				vscode_sessionid TEXT DEFAULT '',
				session_expires_at INTEGER DEFAULT 0,
				last_touched INTEGER DEFAULT 0
			)
		`)
		if err != nil {
			log.Logger.Error().Str("type", "Cache.connect").Msg("Failed to create table: cache")
			panic(err)
		}
		log.Logger.Debug().Str("type", "Cache.connect").Msg("Created table: cache")
	} else if !c.cache && c.data == nil {
		c.data = make(map[string]Item)
		log.Logger.Debug().Str("type", "Cache.connect").Msg("Created cache map in memory")
	}
}

// @description get a record
//
// @param app_token string "The app token"
//
// @return (Item, error) "The item and the error"
func (c *Cache) get(app_token string) (Item, myErr.Error) {
	c.connect()

	switch c.cache {
	case true:
		var item Item
		err := c.db.Get(&item, "SELECT * FROM cache WHERE app_token = ?", app_token)
		// if not found, return empty item
		if err != nil {
			log.Logger.Trace().Str("type", "Cache.get").Msgf("Item not found: %s. Error: %s", app_token, err.Error())
			return Item{}, &myErr.CoErr{
				ErrCode: myErr.ERR_CACHE_ITEM_NOT_EXISTS,
				Msg:     err.Error(),
				Typ:     "Cache.get",
			}
		}
		// if found, return item
		log.Logger.Trace().Str("type", "Cache.get").Msgf("Item found: %s", app_token)
		return item, nil

	case false:
		item, ok := c.data[app_token]
		if !ok {
			log.Logger.Trace().Str("type", "Cache.get").Msgf("Item not found: %s", app_token)
			return Item{}, &myErr.CoErr{
				ErrCode: myErr.ERR_CACHE_ITEM_NOT_EXISTS,
				Msg:     "Item not found",
				Typ:     "Cache.get",
			}
		} else {
			log.Logger.Trace().Str("type", "Cache.get").Msgf("Item found: %s", app_token)
			return item, nil
		}

	default:
		log.Logger.Error().Str("type", "Cache.get").Msg("This should not happen")
		return Item{}, nil
	}
}

// @description update a record
//
// @param
//   - app_token string "The app token"
//   - item Item "The item"
//
// @return error "The error"
func (c *Cache) update(app_token string, item *Item) myErr.Error {
	c.connect()

	// try to get the item
	item_old, err := c.get(app_token)
	switch c.cache {

	case true:
		if err != nil { // if not found, insert
			log.Logger.Trace().Str("type", "Cache.update").Msgf("Item not found, inserting: %s", app_token)
			_, err := c.db.Exec(`
				INSERT INTO cache (app_token, c_token, expires_at, vscode_machineid, vscode_sessionid, session_expires_at, last_touched)
				VALUES (?, ?, ?, ?, ?, ?, ?)`,
				app_token, item.C_token, item.ExpiresAt, item.Vscode_machineid, item.Vscode_sessionid, item.Session_expires_at, item.Last_touched,
			)
			if err != nil {
				log.Logger.Error().Str("type", "Cache.update").Msgf("Failed to insert item: %s. Error: %s", app_token, err.Error())
				return &myErr.CoErr{
					ErrCode: myErr.ERR_UNKNOWN,
					Msg:     err.Error(),
					Typ:     "Cache.update",
				}
			}
			log.Logger.Trace().Str("type", "Cache.update").Msgf("Inserted item: %s", app_token)
		} else { // if found, update
			log.Logger.Trace().Str("type", "Cache.update").Msgf("Item found, updating: %s", app_token)
			// update none zero fields
			item_old.UpdateNoneZeroFields(item)
			_, err := c.db.Exec(`
				UPDATE cache
				SET c_token = ?, expires_at = ?, vscode_machineid = ?, vscode_sessionid = ?, session_expires_at = ?, last_touched = ?
				WHERE app_token = ?`,
				item_old.C_token, item_old.ExpiresAt, item_old.Vscode_machineid, item_old.Vscode_sessionid, item_old.Session_expires_at, item_old.Last_touched, app_token,
			)
			if err != nil {
				log.Logger.Error().Str("type", "Cache.update").Msgf("Failed to update item: %s. Error: %s", app_token, err.Error())
				return &myErr.CoErr{
					ErrCode: myErr.ERR_UNKNOWN,
					Msg:     err.Error(),
					Typ:     "Cache.update",
				}
			}
			log.Logger.Trace().Str("type", "Cache.update").Msgf("Updated item: %s", app_token)
		}

	case false:
		if err != nil { // if not found, insert
			log.Logger.Trace().Str("type", "Cache.update").Msgf("Item not found, inserting: %s", app_token)
			c.data[app_token] = *item
			log.Logger.Trace().Str("type", "Cache.update").Msgf("Inserted item: %s", app_token)
		} else { // if found, update
			log.Logger.Trace().Str("type", "Cache.update").Msgf("Item found, updating: %s", app_token)
			// update none zero fields
			item_old.UpdateNoneZeroFields(item)
			c.data[app_token] = item_old
		}
		log.Logger.Trace().Str("type", "Cache.update").Msgf("Updated item: %s", app_token)

	default:
		log.Logger.Error().Str("type", "Cache.update").Msg("This should not happen")
		return nil
	}

	return nil
}

// @description update sessionid or machineid if necessary
//
// @param app_token string "The app token"
//
// @return error "The error"
func (c *Cache) routine(app_token string) myErr.Error {
	c.connect()

	log.Logger.Debug().Str("type", "Cache.routine").Msgf("Start routine for: %s", app_token)
	to_update := Item{Last_touched: time.Now().Unix()}
	item, err := c.get(app_token)
	if err != nil {
		log.Logger.Error().Str("type", "Cache.routine").Msgf("Failed to get item: %s. Error: %s", app_token, err.Error())
		return err
	}
	// if sessionid is expired, update it
	if item.Session_expires_at < time.Now().Unix() {
		log.Logger.Debug().Str("type", "Cache.routine").Msgf("Sessionid expired, updating: %s", app_token)
		to_update.Vscode_sessionid = util.GenSessionID()
		to_update.Session_expires_at = time.Now().Unix() + SESSIONID_TTL
		log.Logger.Debug().Str("type", "Cache.routine").Msgf("Updated sessionid: %s", app_token)
	}
	if item.Vscode_machineid == "" {
		log.Logger.Debug().Str("type", "Cache.routine").Msgf("Machineid not found, creating: %s", app_token)
		to_update.Vscode_machineid = util.GenMachineID(util.UUID)
		log.Logger.Debug().Str("type", "Cache.routine").Msgf("Created machineid: %s", app_token)
	}

	log.Logger.Debug().Str("type", "Cache.routine").Msgf("Routine finished for: %s", app_token)

	return c.update(app_token, &to_update)
}

// @description Delete a record
//
// @param app_token string "The app token"
//
// @return error "The error"
func (c *Cache) Delete(app_token string) error {
	c.connect()

	var err error = nil

	switch c.cache {
	case true:
		_, err = c.db.Exec("DELETE FROM cache WHERE app_token = ?", app_token)

	case false:
		delete(c.data, app_token)

	default:
		log.Logger.Error().Str("type", "Cache.Delete").Msg("This should not happen")
		return nil
	}

	if err != nil {
		log.Logger.Error().Str("type", "Cache.Delete").Msgf("Failed to delete item: %s. Error: %s", app_token, err.Error())
	} else {
		log.Logger.Debug().Str("type", "Cache.Delete").Msgf("Deleted item: %s", app_token)
	}

	return err
}

// @description Clean items that have not been touched for a long time
func (c *Cache) clean() {
	c.connect()

	log.Logger.Debug().Str("type", "Cache.clean").Msg("Start cleaning")

	switch c.cache {
	case true:
		var items []Item
		err := c.db.Select(&items, `
			SELECT * FROM cache
			WHERE last_touched < ?`,
			time.Now().Unix()-CLEAN_TIMEOUT,
		)
		if err == nil {
			for _, item := range items {
				log.Logger.Debug().Str("type", "Cache.clean").Msgf("Deleting item: %s", item.App_token)
				err := c.Delete(item.App_token)
				if err != nil {
					log.Logger.Error().Str("type", "Cache.clean").Msgf("Failed to delete item: %s. Error: %s", item.App_token, err.Error())
					panic(err)
				}
			}
		}

	case false:
		for app_token, item := range c.data {
			if item.Last_touched < time.Now().Unix()-CLEAN_TIMEOUT {
				delete(c.data, app_token)
			}
		}

	default:
		log.Logger.Error().Str("type", "Cache.clean").Msg("This should not happen")
	}

	log.Logger.Debug().Str("type", "Cache.clean").Msg("Cleaning finished")
}

// @description Get a record
//
// @param app_token string "The app token"
//
// @return (Item, error) "The item and the error"
func (c *Cache) Get(app_token string) (Item, myErr.Error) {
	var item Item
	var err error
	_, err = c.get(app_token)
	if err != nil {
		log.Logger.Debug().Str("type", "Cache.Get").Msgf("Item not found: %s", app_token)
		return Item{}, &myErr.CoErr{
			ErrCode: myErr.ERR_CACHE_ITEM_NOT_EXISTS,
			Msg:     err.Error(),
			Typ:     "Cache.Get",
		}
	}
	err = c.routine(app_token)
	if err != nil {
		log.Logger.Error().Str("type", "Cache.Get").Msgf("Failed to update sessionid or machineid: %s. Error: %s", app_token, err.Error())
		return Item{}, &myErr.CoErr{
			ErrCode: myErr.ERR_UNKNOWN,
			Msg:     err.Error(),
			Typ:     "Cache.Get",
		}
	}
	item, err = c.get(app_token)
	if err != nil {
		log.Logger.Error().Str("type", "Cache.Get").Msgf("Failed to get item: %s. Error: %s", app_token, err.Error())
		return Item{}, &myErr.CoErr{
			ErrCode: myErr.ERR_UNKNOWN,
			Msg:     err.Error(),
			Typ:     "Cache.Get",
		}
	}
	return item, nil
}

// @description Set a record
//
// @param
//   - app_token string "The app token"
//   - item Item "The item"
//
// @return error "The error"
func (c *Cache) Set(app_token string, item *Item) myErr.Error {
	now := time.Now().Unix()
	// try to get the item
	item_old, err := c.get(app_token)

	if err != nil { // if not found, insert
		log.Logger.Debug().Str("type", "Cache.Set").Msgf("Item not found, inserting: %s", app_token)
		item_old = Item{
			App_token:        app_token,
			C_token:          item.C_token,
			ExpiresAt:        item.ExpiresAt,
			Vscode_machineid: item.Vscode_machineid,
			Last_touched:     now,
		}
	} else { // if found, update
		log.Logger.Debug().Str("type", "Cache.Set").Msgf("Item found, updating: %s", app_token)
		if item.C_token != "" {
			item_old.C_token = item.C_token
		}
		if item.ExpiresAt != 0 {
			item_old.ExpiresAt = item.ExpiresAt
		}
		if item.Vscode_machineid != "" {
			item_old.Vscode_machineid = item.Vscode_machineid
		}
		item_old.Last_touched = now
	}

	// update
	err = c.update(app_token, &item_old)
	if err != nil {
		log.Logger.Error().Str("type", "Cache.Set").Msgf("Failed to update item: %s. Error: %s", app_token, err.Error())
		return err
	}
	log.Logger.Debug().Str("type", "Cache.Set").Msgf("Updated item: %s", app_token)

	// update sessionid or machineid if necessary
	return c.routine(app_token)
}

// @description Close the database
func (c *Cache) Close() {
	switch c.cache {
	case true:
		if c.db != nil {
			// clean
			c.clean()
			c.db.Close()
			c.db = nil
		}

	case false:
		c.clean()
		c.data = nil

	default:
	}

	log.Logger.Debug().Str("type", "Cache.Close").Msg("Closed")
}
