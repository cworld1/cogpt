package cache

type Item struct {
	App_token          string `db:"app_token"`
	C_token            string `db:"c_token,omitempty"`
	ExpiresAt          int64  `db:"expires_at,omitempty"`
	Vscode_machineid   string `db:"vscode_machineid,omitempty"`
	Vscode_sessionid   string `db:"vscode_sessionid,omitempty"`
	Session_expires_at int64  `db:"session_expires_at,omitempty"`
	Last_touched       int64  `db:"last_touched,omitempty"`
}

func (i *Item) UpdateNoneZeroFields(another *Item) {
	if another.App_token != "" {
		i.App_token = another.App_token
	}
	if another.C_token != "" {
		i.C_token = another.C_token
	}
	if another.ExpiresAt != 0 {
		i.ExpiresAt = another.ExpiresAt
	}
	if another.Vscode_machineid != "" {
		i.Vscode_machineid = another.Vscode_machineid
	}
	if another.Vscode_sessionid != "" {
		i.Vscode_sessionid = another.Vscode_sessionid
	}
	if another.Session_expires_at != 0 {
		i.Session_expires_at = another.Session_expires_at
	}
	if another.Last_touched != 0 {
		i.Last_touched = another.Last_touched
	}
}
