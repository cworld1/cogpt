package share

import (
	"cogpt/internal/config"
	"cogpt/internal/log"
	"strings"
)

// @description Get Real Token
//
// @param
//   - token string "token from request"
//
// @return string "real token"
func GetRealToken(token string) (string, bool) {
	if strings.HasPrefix(token, "ghu_") {
		return token, true
	} else if strings.HasPrefix(token, "share-") {
		realToken, ok := config.ConfigInstance.ShareToken[token]
		return realToken, ok
	}
	log.Logger.Debug().Str("type", "GetRealToken").Msgf("token not start with ghu_ or share-, token: %s", token)
	return "", false
}
