package cogpt

type CompletionsMessage struct {
	Role    *string `json:"role,omitempty"`
	Content string  `json:"content"`
}

type CompletionsChoice struct {
	Delta         *CompletionsMessage `json:"delta,omitempty"`
	Message       *CompletionsMessage `json:"message,omitempty"`
	Index         int                 `json:"index"`
	Finish_reason *string             `json:"finish_reason"`
}

type EmbeddingsData struct {
	Object    string `json:"object"`
	Index     int    `json:"index"`
	Embedding any    `json:"embedding"`
}

type Usage struct {
	Prompt_tokens     int  `json:"prompt_tokens"`
	Completion_tokens *int `json:"completion_tokens,omitempty"`
	Total_tokens      int  `json:"total_tokens"`
}

type CompletionsResponse struct {
	Choices []CompletionsChoice `json:"choices"`
	Created int64               `json:"created"`
	ID      string              `json:"id"`
	Model   string              `json:"model"`
	Object  string              `json:"object"`
	Usage   *Usage              `json:"usage,omitempty"`
}

type EmbeddingsResponse struct {
	Object string           `json:"object"`
	Data   []EmbeddingsData `json:"data"`
	Model  string           `json:"model"`
	Usage  *Usage           `json:"usage,omitempty"`
}

type CompletionsRequest struct {
	Messages    []CompletionsMessage `json:"messages"`
	Model       string               `json:"model"`
	Temperature float64              `json:"temperature"`
	Top_p       float64              `json:"top_p"`
	N           int64                `json:"n"`
	Stream      bool                 `json:"stream"`
}

type EmbeddingsRequest struct {
	Input any    `json:"input"`
	Model string `json:"model"`
}

type Authorization struct {
	Token     string `json:"token"`
	ExpiresAt int64  `json:"expires_at"`
}
