package proxy

import (
	"testing"
)

func TestGetEnvProxyAddress(t *testing.T) {
	tests := []struct {
		url   string
		ifErr bool
	}{
		{"http://localhost:8080", false},
		{"https://localhost:8080", false},
		{"socks5://localhost:8080", false},
		{"http://localhost", true},
		{"socks5://[::1]:8080", false},
		{"socks5://::1:8080", true},
		{"socks5://example.com:8080", false},
		{"socks5://exa-mple.com:8080", false},
	}
	for _, test := range tests {
		if _, err := genTransport(test.url); (err != nil) != test.ifErr {
			t.Errorf("genTransport(%s) = %v, want %v", test.url, err, test.ifErr)
		}
	}
}
