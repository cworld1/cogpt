package main

import (
	"cogpt/internal/config"
	"cogpt/internal/log"
	"fmt"
	"reflect"
	"strconv"

	_ "cogpt/internal/init"

	"github.com/gin-gonic/gin"
)

func main() {
	// print config using reflection
	elem := reflect.ValueOf(&config.ConfigInstance).Elem()
	typeOfElem := elem.Type()
	for i := 0; i < elem.NumField(); i++ {
		log.Logger.Info().Str("type", "config").Msgf("%s: %v", typeOfElem.Field(i).Name, elem.Field(i).Interface())
	}
	fmt.Println()

	if config.ConfigInstance.Debug {
		gin.SetMode(gin.DebugMode)
	} else {
		gin.SetMode(gin.ReleaseMode)
	}

	// new router
	router := gin.New()
	router.Use(LoggerMiddleware(), gin.Recovery())
	router.Use(CORSMiddleware())
	// set path
	router.GET("/", handlerRoot)
	router.GET("/health", handlerHealth)
	router.GET("/robots.txt", handlerRobots)
	router.GET("/v1/models", handlerV1Models)
	router.POST("/v1/chat/completions", handlerV1ChatCompletions)
	router.POST("/v1/embeddings", handlerV1Embeddings)

	log.Logger.Info().Str("type", "startup").Msg("Start server on " + config.ConfigInstance.Host + ":" + strconv.Itoa(config.ConfigInstance.Port))
	fmt.Println()

	// start router
	var err error
	if config.ConfigInstance.CertFile != "" {
		err = router.RunTLS(config.ConfigInstance.Host+":"+strconv.Itoa(config.ConfigInstance.Port), config.ConfigInstance.CertFile, config.ConfigInstance.KeyFile)
	} else {
		err = router.Run(config.ConfigInstance.Host + ":" + strconv.Itoa(config.ConfigInstance.Port))
	}

	if err != nil {
		log.Logger.Fatal().Str("type", "startup").Msg(err.Error())
	}
}
