package main

import (
	"github.com/gin-gonic/gin"
)

// @description abort with error
func abortWithError(c *gin.Context, httpStatus int, errorMsg string) {
	c.AbortWithStatusJSON(
		httpStatus,
		gin.H{
			"error": errorMsg,
			"code":  httpStatus,
		},
	)
}
