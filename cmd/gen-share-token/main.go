package main

import (
	"cogpt/internal/util"
	"fmt"
)

func main() {
	fmt.Println("A random share token:")
	fmt.Println("share-" + util.GenRandomHex(36))
	fmt.Println()
	fmt.Print("Press 'Enter' to exit...")
	fmt.Scanln()
}
