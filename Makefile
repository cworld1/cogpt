exeargs =
args = 
args1 =
args2 =

ifeq ($(OS),Windows_NT)
    EXE_EXT = .exe
else
    EXE_EXT =
endif

.PHONY: run
run: cogpt-api$(EXE_EXT)
	./cogpt-api $(exeargs)

.PHONY: all
all: cogpt-api$(EXE_EXT) cogpt-get-apptoken$(EXE_EXT) gen-share-token$(EXE_EXT)

cogpt-api$(EXE_EXT): cmd/cogpt-api/*.go internal/*/*.go
	$(args1) go build $(args) ./cmd/cogpt-api $(args2)

cogpt-get-apptoken$(EXE_EXT): cmd/cogpt-get-apptoken/*.go internal/*/*.go
	$(args1) go build $(args) ./cmd/cogpt-get-apptoken $(args2)

gen-share-token$(EXE_EXT): cmd/gen-share-token/*.go internal/*/*.go
	$(args1) go build $(args) ./cmd/gen-share-token $(args2)

.PHONY: tests
tests:
	go test ./...

.PHONY: clean
clean:
	rm -rf ./cogpt-api$(EXE_EXT) ./cogpt-get-apptoken$(EXE_EXT) ./gen-share-token$(EXE_EXT)
